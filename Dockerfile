FROM alpine:latest

#TODO(yyavarouski): add minify tools

COPY images /var/local/images
COPY styles /var/local/styles
COPY templates /var/local/templates

ENTRYPOINT cp -r /var/local/* /srv/www/
